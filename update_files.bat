@echo off

set TZ1_BASE=C:\Cerevo\CDP-TZ01B\
set INSTALL_FILES=install_files\

set BACKUP_DIR=%TZ1_BASE%backup_%DATE:~-10,4%%DATE:~-5,2%%DATE:~-2%-%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%

md %BACKUP_DIR%

move /Y %TZ1_BASE%reference       %BACKUP_DIR%\
move /Y %TZ1_BASE%scripts         %BACKUP_DIR%\
move /Y %TZ1_BASE%skel            %BACKUP_DIR%\
move /Y %TZ1_BASE%start_shell.bat %BACKUP_DIR%\

xcopy /Y /E _TZ1\* %TZ1_BASE%
pause
