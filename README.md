# BlueNinja(Cerevo CDP-TZ01B) BSP #

This project is the Boad Support Package for [BlueNinja](https://blueninja.cerevo.com/).  
Also it includes development environment build script for Windows.

## Required packages
* [CMSIS Version 3.20.4](http://www.keil.com/dd2/pack/)
* [TOSHIBA TZ1000 BSP(TOSHIBA.TZ10xx_DFP)](http://toshiba.semicon-storage.com/jp/design-support/login/applite.html)  
  Download of the TZ1000 BSP, must sign to SULA(Start-Up License Agreement).

## Change history
### RELEASE_160615
#### Changes:
* Add file update script.  
  "update_files.bat" is backup installed files and copy new files.

#### Supported TOSHIBA.TZ10xx_DFP versions:
* 1.39.0
* 1.35.0
* 1.31.1

### RELEASE_160412
#### Changes:
* Add TOSHIBA.TZ10xx_DFP version 1.39.0 support.
* Update BlueNinja library
    - TZ01_system.c(TZ01_system_tick_start()): Add support to SystemCoreClock other than 48MHz.
    - MPU-9250.c/MPU-9250.h: Add sleep mode support.
    - MPU-9250.c/MPU-9250.h: Add Start / Stop maesurement functions of Gyro, Accelometer, Magnetometer.

#### Supported TOSHIBA.TZ10xx_DFP versions:
* 1.39.0
* 1.35.0
* 1.31.1

### RELEASE_151021
#### Changes:
* Add multiple versions of TOSHIBA.TZ10xx_DFP support.
    - Add supported version of TOSHIBA.TZ10xx_DFP pack deployment feature in the install scripts.
    - Add DFP_VER(TOSHIBA.TZ10xx_DFP version variable) to Makefile skeleton.
    - Add version selection option to 'app_create.bat' script.
* Update BlueNinja library
    - MPU-9250.c: Add to reset the built-in I2C controller slave device initializarion process.
    - BMP280.c: Chage I2C bus speed of to 'STANDARD'.
    - BQ24250.c: Change I2C bus speed of to 'STANDARD'.
    - TZ01_motion_tracker.c: Fixed mistakes paramters of the 'MPU9250_drv_start_maesure()' the call.

#### Supported TOSHIBA.TZ10xx_DFP versions:
* 1.35.0
* 1.31.1

### RELEASE_150929
#### Canges:
* Win64: Fix a problem that dose not support to the 64 bit version of Git for Windows.
* Win32: Fix the install problem.
#### Supported TOSHIBA.TZ10xx_DFP versions:
* 1.31.1

### RELEASE_150806
* First release.

#### Supported TOSHIBA.TZ10xx_DFP versions:
* 1.31.1
