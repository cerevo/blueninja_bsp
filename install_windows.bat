@echo off
rem Path configs.
set TZ1_BASE=C:\Cerevo\CDP-TZ01B\
set INSTALL_FILES=install_files\
set UTIL_PATH=""

set GCC_ARM_NONE_EABI=gcc-arm-none-eabi-4_9-2015q1-20150306-win32.zip
set GCC_ARM_NONE_EABI_URL=https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q1-update/+download/gcc-arm-none-eabi-4_9-2015q1-20150306-win32.zip
set OPEN_OCD=OpenOCD-0.8.0-Win32.zip
set OPEN_OCD_URL=http://sysprogs.com/files/gnutoolchains/arm-eabi/openocd/OpenOCD-0.8.0-Win32.zip

if %UTIL_PATH% == "" (
	if exist "%ProgramFiles%\Git\usr\bin" (
		rem Git for Windows 2.x
		set UTIL_PATH="%ProgramFiles%\Git\usr\bin"
	) else if exist "%ProgramFiles(x86)%\Git\usr\bin" (
		rem Git for Windows 2.x Wow
		set UTIL_PATH="%ProgramFiles(x86)%\Git\usr\bin"
	) else if exist "%ProgramFiles%\Git\bin" (
		rem Git for Windows 1.x
		set UTIL_PATH="%ProgramFiles%\Git\bin"
	) else if exist "%ProgramFiles(x86)%\Git\bin" (
		rem Git for Windows 1.x Wow
		set UTIL_PATH="%ProgramFiles(x86)%\Git\bin"
	)
)
rem PATH setting.
PATH=%SystemRoot%;%SystemRoot%\System32
if exist %UTIL_PATH% (
	set PATH=%PATH%;%UTIL_PATH%
) else (
	echo "Git for Windows not installed."
	pause
	exit
)

rem Create directories.
if not exist "%TZ1_BASE%" (
	mkdir "%TZ1_BASE%"
)

echo Setup SDKs
if not exist "%TZ1_BASE%sdk" (
	mkdir "%TZ1_BASE%sdk"
)
echo Extract TZ10xx_DFP
call append_pack.bat
set CNT=%errorlevel%

cd %INSTALL_FILES%

if not exist "%TZ1_BASE%sdk\TOSHIBA.TZ10xx_DFP" (
	mkdir "%TZ1_BASE%sdk\TOSHIBA.TZ10xx_DFP"
)

if exist "TOSHIBA.TZ10xx_DFP.1.31.1.pack" (
	unzip -o -q -d %TZ1_BASE%sdk\TOSHIBA.TZ10xx_DFP TOSHIBA.TZ10xx_DFP.1.31.1.pack
	set /A CNT+=1
)

if %CNT%==0 (
	echo "Error: TOSHIBA.TZ10xx_DFP.*.pack not installed."
	exit
)

echo Extract CMSIS
if not exist "%TZ1_BASE%sdk\ARM.CMSIS" (
	mkdir "%TZ1_BASE%sdk\ARM.CMSIS"
)

if exist "ARM.CMSIS.3.20.4.pack" (
	unzip -o -q -d %TZ1_BASE%sdk\ARM.CMSIS ARM.CMSIS.3.20.4.pack
) else if exist "ARM.CMSIS.3.20.4.zip" (
	unzip -o -q -d %TZ1_BASE%sdk\ARM.CMSIS ARM.CMSIS.3.20.4.zip
) else (
	echo "ARM.CMSIS.3.20.4 is notfound."
	exit
)

echo Setup tool chain.
if not exist "%TZ1_BASE%tools" (
	mkdir "%TZ1_BASE%tools"
)

if not exist "%GCC_ARM_NONE_EABI%" (
	echo Download gcc-arm-none-eabi
	curl -fLO %GCC_ARM_NONE_EABI_URL%
	if NOT %ERRORLEVEL% == 0 (
		goto DL_FAILED
	)
)
echo Extract gcc-arm-none-eabi
unzip -o -q -d %TZ1_BASE%tools %GCC_ARM_NONE_EABI%
if NOT %ERRORLEVEL% == 0 (
	del /Q %GCC_ARM_NONE_EABI%
	goto EX_FAILED
)

echo GNU tool TZ10xx Support
copy tz10xx.specs %TZ1_BASE%tools\arm-none-eabi\lib
%TZ1_BASE%tools\bin\arm-none-eabi-gcc.exe -mcpu=cortex-m4 -mthumb -mthumb-interwork -march=armv7e-m -mfloat-abi=soft -std=c99 -g -O0 -c tz10xx-crt0.c -o %TZ1_BASE%tools\arm-none-eabi\lib\tz10xx-crt0.o

if not exist "%OPEN_OCD%" (
	echo Download OpenOCD
	curl -fLO %OPEN_OCD_URL%
	if NOT %ERRORLEVEL% == 0 (
		goto DL_FAILED
	)
)
echo Extract OpenOCD
unzip -o -q -d %TZ1_BASE% %OPEN_OCD%
if NOT %ERRORLEVEL% == 0 (
	del /Q %OPEN_OCD%
	goto EX_FAILED
)

copy /Y tz10xx.cfg %TZ1_BASE%OpenOCD-0.8.0\share\openocd\scripts\target
copy /Y tz10xx_reset.tcl %TZ1_BASE%OpenOCD-0.8.0\share\openocd\scripts\target

echo Setup Scripts.
cd ..\
xcopy /Y /E _TZ1\* %TZ1_BASE%

echo DONE
pause

exit /b

:DL_FAILED
echo **ERROR** Download failed.
echo Please rerun the "install_windows.bat".
pause
exit /b

:EX_FAILED
echo **ERROR** Extract failed.
echo Please rerun the "install_windows.bat".
pause
exit /b

