@echo off

set TZ1_BASE=C:\Cerevo\CDP-TZ01B\
set INSTALL_FILES=install_files\
set TOOLS_PATH="C:\Program Files\Git\usr\bin"

PATH=%SystemRoot%;%SystemRoot%\System32;%TOOLS_PATH%

if not exist "%TZ1_BASE%sdk" (
	mkdir "%TZ1_BASE%sdk"
)

set CNT=0
for %%f in (versions\*.patch) do (
	echo Extracting TOSHIBA.TZ10xx_DFP.%%~nf.pack
	if exist "%INSTALL_FILES%TOSHIBA.TZ10xx_DFP.%%~nf.pack" (
		unzip -o -q -d "%TZ1_BASE%sdk\TOSHIBA.TZ10xx_DFP.%%~nf" "%INSTALL_FILES%TOSHIBA.TZ10xx_DFP.%%~nf.pack"
		copy %%f "%TZ1_BASE%sdk"
		set /A CNT+=1
	)
)
echo %CNT% packs install succeed.

echo Applying for patchs.
for %%f in (%INSTALL_FILES%TOSHIBA.TZ10xx_DFP*.patch) do (
	if exist %TZ1_BASE%sdk\%%~nf (
		echo %%f
		patch -p0 -d %TZ1_BASE%sdk < %%f
	)
)

exit /b %CNT%
