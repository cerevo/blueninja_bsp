var _m_p_u_9250_8c =
[
    [ "MPU9250_STAT", "db/d4e/_m_p_u-9250_8c.html#a3d2d5e99fb3428346275446542edc8f4", [
      [ "MPU9250_STAT_NONE", "db/d4e/_m_p_u-9250_8c.html#a3d2d5e99fb3428346275446542edc8f4a4930544829657b2d1469b29ef050e801", null ],
      [ "MPU9250_STAT_IDLE", "db/d4e/_m_p_u-9250_8c.html#a3d2d5e99fb3428346275446542edc8f4a978e65cfd606c43c152a42b337ca0ec2", null ],
      [ "MPU9250_STAT_MAESUREING", "db/d4e/_m_p_u-9250_8c.html#a3d2d5e99fb3428346275446542edc8f4a48cb8b3d0a845f4cded8a96be227d778", null ]
    ] ],
    [ "MPU9250_drv_disable_events", "db/d4e/_m_p_u-9250_8c.html#a4dea092bead3c5ecfa3bcf2adc419fc6", null ],
    [ "MPU9250_drv_enable_events", "db/d4e/_m_p_u-9250_8c.html#accb8ac878b729a0b7087bd8ad5d9fec1", null ],
    [ "MPU9250_drv_init", "db/d4e/_m_p_u-9250_8c.html#a0c2298502a25038e67cdae776fb62871", null ],
    [ "MPU9250_drv_read_accel", "db/d4e/_m_p_u-9250_8c.html#a0e5b1da4ef39091f8311410f66d9e13b", null ],
    [ "MPU9250_drv_read_gyro", "db/d4e/_m_p_u-9250_8c.html#a932089d2a2347dd27efb9bcb25b3eff9", null ],
    [ "MPU9250_drv_read_magnetometer", "db/d4e/_m_p_u-9250_8c.html#a8a18203f1e1c42a4aeecc7a503722949", null ],
    [ "MPU9250_drv_read_temperature", "db/d4e/_m_p_u-9250_8c.html#a29264826438e36ff8aa46ef1ba2087fb", null ],
    [ "MPU9250_drv_sleep", "db/d4e/_m_p_u-9250_8c.html#a9cbac6768f39a0d3d87f429067efd0c6", null ],
    [ "MPU9250_drv_start_accel", "db/d4e/_m_p_u-9250_8c.html#aa4c8442e09b6496e7c461f6956e15905", null ],
    [ "MPU9250_drv_start_gyro", "db/d4e/_m_p_u-9250_8c.html#a61952362618ff6b936200e148b567465", null ],
    [ "MPU9250_drv_start_lowpower_wom", "db/d4e/_m_p_u-9250_8c.html#a77334dd7139b5c4366fee4a42c0cae2e", null ],
    [ "MPU9250_drv_start_maesure", "db/d4e/_m_p_u-9250_8c.html#a12a4a4fd1256d37c165a797d54c61d5e", null ],
    [ "MPU9250_drv_start_magnetometer", "db/d4e/_m_p_u-9250_8c.html#ac7397c0a9faa170a887f4f739290c0bf", null ],
    [ "MPU9250_drv_stop_accel", "db/d4e/_m_p_u-9250_8c.html#adc2a2f9b9c6b1c6c6d94146c9e61bcee", null ],
    [ "MPU9250_drv_stop_gyro", "db/d4e/_m_p_u-9250_8c.html#aa1363de8dd407501b4b48e07483771c3", null ],
    [ "MPU9250_drv_stop_maesure", "db/d4e/_m_p_u-9250_8c.html#ad9cd4deb4d7f4f70ccd15841914cff33", null ],
    [ "MPU9250_drv_stop_magnetometer", "db/d4e/_m_p_u-9250_8c.html#a34faadec80e7ef108a5365cd7a2e96df", null ],
    [ "MPU9250_drv_wakeup", "db/d4e/_m_p_u-9250_8c.html#ac09b11b3a29635f53a083f182fab51a8", null ],
    [ "Driver_GPIO", "db/d4e/_m_p_u-9250_8c.html#a7f939f641c0cf86acedf9f83650b2abe", null ],
    [ "Driver_PMU", "db/d4e/_m_p_u-9250_8c.html#a5f4e5a61324b94332b45b66d2b060a67", null ]
];