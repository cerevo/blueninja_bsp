var _b_q24250_8c =
[
    [ "BQ24250_drv_init", "de/d1f/_b_q24250_8c.html#a335c7d217fbff18d29601f5f2cf997c7", null ],
    [ "BQ24250_drv_reg01_get", "de/d1f/_b_q24250_8c.html#a1d0fb7bbce391d2d0f317b29c0f7c2ad", null ],
    [ "BQ24250_drv_reg01_set", "de/d1f/_b_q24250_8c.html#a751e86b9d49dada265c7825747630f06", null ],
    [ "BQ24250_drv_reg02_get", "de/d1f/_b_q24250_8c.html#ab3e01600e342f011e193bd0e38b8fb01", null ],
    [ "BQ24250_drv_reg02_set", "de/d1f/_b_q24250_8c.html#afaf5f902022703e1cfb0367d6fdc8b76", null ],
    [ "BQ24250_drv_reg03_get", "de/d1f/_b_q24250_8c.html#acb20e468afe8d0ae49d4ed853d2cf44c", null ],
    [ "BQ24250_drv_reg03_set", "de/d1f/_b_q24250_8c.html#ae1aef98d98f4731aebfd471384464552", null ],
    [ "BQ24250_drv_reg04_get", "de/d1f/_b_q24250_8c.html#a8f82f7bcac53cbe331e56975ab9864e5", null ],
    [ "BQ24250_drv_reg04_set", "de/d1f/_b_q24250_8c.html#ad864a0153b9adce6fe55a8a7fbf5fa09", null ],
    [ "BQ24250_drv_reg05_get", "de/d1f/_b_q24250_8c.html#a722e3849b43b680553d59c795cd93de3", null ],
    [ "BQ24250_drv_reg05_set", "de/d1f/_b_q24250_8c.html#a8d173778d03a696d4276510cc62c2cc8", null ],
    [ "BQ24250_drv_reg06_get", "de/d1f/_b_q24250_8c.html#a7ceadb8f8185325cac08a5e5ec64f270", null ],
    [ "BQ24250_drv_reg06_set", "de/d1f/_b_q24250_8c.html#ad7e06335e613c259116e1c921269974d", null ],
    [ "BQ24250_drv_reg07_get", "de/d1f/_b_q24250_8c.html#a91edbdc3f25080a858f4c4f50b63bc3d", null ],
    [ "BQ24250_drv_reg07_set", "de/d1f/_b_q24250_8c.html#a7059253363dee0285fb6e7a042eaeb91", null ]
];