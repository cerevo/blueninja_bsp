var _t_z01__motion__tracker_8h =
[
    [ "PI", "d7/dc4/_t_z01__motion__tracker_8h.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "TZ01_motion_tracker_acc_axis_angle", "d7/dc4/_t_z01__motion__tracker_8h.html#a87cbbf9bbced117867be6b87385b4eed", null ],
    [ "TZ01_motion_tracker_accel_read", "d7/dc4/_t_z01__motion__tracker_8h.html#a91a96ddab55bd31bdc190ac6ebddeed4", null ],
    [ "TZ01_motion_tracker_compute_axis_angle", "d7/dc4/_t_z01__motion__tracker_8h.html#a6781f243f1e8c57012aa9f2802474b5b", null ],
    [ "TZ01_motion_tracker_gyro_read", "d7/dc4/_t_z01__motion__tracker_8h.html#a5647d005e2acecb8d8fff2dbec41f170", null ],
    [ "TZ01_motion_tracker_init", "d7/dc4/_t_z01__motion__tracker_8h.html#a2e0eb356a8098d4aa621183e20c51c00", null ],
    [ "TZ01_motion_tracker_magnetometer_read", "d7/dc4/_t_z01__motion__tracker_8h.html#a51e761260bce5db98909689ddb109e0a", null ],
    [ "TZ01_motion_tracker_temperature_read", "d7/dc4/_t_z01__motion__tracker_8h.html#a80e8b35695476598136fd69b2e3ecca2", null ]
];