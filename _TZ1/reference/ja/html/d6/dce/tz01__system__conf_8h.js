var tz01__system__conf_8h =
[
    [ "TZ01_CONSOLE_BAUD", "d6/dce/tz01__system__conf_8h.html#a36d17a5f061c896bd66d18dee63f8416", null ],
    [ "TZ01_CONSOLE_ENABLE", "d6/dce/tz01__system__conf_8h.html#a3415fad28e8e9d7a5bec5a7776685893", null ],
    [ "TZ01_CONSOLE_UART_CH", "d6/dce/tz01__system__conf_8h.html#a147e6a28a9b1007c9e5edce2d25a281d", null ],
    [ "TZ01_HARTBEAT", "d6/dce/tz01__system__conf_8h.html#ab81445996708e5b67f1eceb95782a7ab", null ],
    [ "TZ01_POW_MGR", "d6/dce/tz01__system__conf_8h.html#af1e6daaa7e5879f25188473ff94530dd", null ],
    [ "TZ01_SYSTEM_PW_HLD_DELAY", "d6/dce/tz01__system__conf_8h.html#abe8065069bd76356fb88a71719c5f19f", null ],
    [ "TZ01_SYSTEM_PW_OFF_CNT", "d6/dce/tz01__system__conf_8h.html#a9bce12a58435417deec8df90a95dfd1d", null ],
    [ "TZ01_SYSTEM_PWSW_CHK_INTVAL", "d6/dce/tz01__system__conf_8h.html#aab1a4a3ed86d04228adc91cfa8d84eed", null ],
    [ "TZ01_SYSTEM_PWSW_PORT_HLD", "d6/dce/tz01__system__conf_8h.html#a7cb7c4f6654904f142d0d7ea4d882fe1", null ],
    [ "TZ01_SYSTEM_PWSW_PORT_LED", "d6/dce/tz01__system__conf_8h.html#af912e163fbcb0f855658f6f80c9a450a", null ],
    [ "TZ01_SYSTEM_PWSW_PORT_SW", "d6/dce/tz01__system__conf_8h.html#a3d2e46f7afe1f5c81d7a43d1eacd93bf", null ],
    [ "TZ01_SYSTEM_PWSW_PORT_UVD", "d6/dce/tz01__system__conf_8h.html#a03178e96e247b8d6374d787c9d5bf076", null ],
    [ "TZ01_SYSTEM_TICK_TMR_NO", "d6/dce/tz01__system__conf_8h.html#a4620d58b6a8e50b27561c835bf929fbd", null ],
    [ "TZ01_SYSTEM_TICK_NO", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480", [
      [ "SYSTICK_NO_LED_BLINK", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480a69902771a2d5d46ca27162d90008df65", null ],
      [ "SYSTICK_NO_PWSW_CHECK", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480ad65e0da738ce418697e04c7f3a3da605", null ],
      [ "USRTICK_NO_GPIO_INTERVAL", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480a9a5ea655e2b3586887e55ef1c228312e", null ],
      [ "USRTICK_NO_BLE_MAIN", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480ab1b1ff3e1431dba495980e9932fcee79", null ],
      [ "_TICK_NO_COUNT", "d6/dce/tz01__system__conf_8h.html#a7d879f4d31c2d2c9b05887027074f480a54e692bfa821aa83d7d87f1cc71f1431", null ]
    ] ]
];