var _b_q24250_8h =
[
    [ "BQ24250_DEF_01", "dd/d35/_b_q24250_8h.html#a5f8885d9325b3949c6ff1d95378f2aab", null ],
    [ "BQ24250_DEF_02", "dd/d35/_b_q24250_8h.html#a526896357a889dc578e17a0e067c1d0e", null ],
    [ "BQ24250_DEF_03", "dd/d35/_b_q24250_8h.html#a40b9aea87695dc30e2407b9b4a8c7ce3", null ],
    [ "BQ24250_DEF_04", "dd/d35/_b_q24250_8h.html#a84574894fb96453f7fdf29baa4918810", null ],
    [ "BQ24250_DEF_05", "dd/d35/_b_q24250_8h.html#aba423fa3882167185fcb40b6669f08f5", null ],
    [ "BQ24250_DEF_06", "dd/d35/_b_q24250_8h.html#a9f45d1f3c4588c6f2f9432f97ebbc257", null ],
    [ "BQ24250_DEF_07", "dd/d35/_b_q24250_8h.html#a9286f60b58dbb493d602c91718cff7d8", null ],
    [ "BQ24250_I2C_ID", "dd/d35/_b_q24250_8h.html#a3098c6298a0f2979e2fa2ce46600c6ec", null ],
    [ "BQ24250_REG_01", "dd/d35/_b_q24250_8h.html#a4ce8ab255280ce1ff0715fa4d8b9d654", null ],
    [ "BQ24250_REG_02", "dd/d35/_b_q24250_8h.html#a93b10081ac2d65dfccc41a04ad44f698", null ],
    [ "BQ24250_REG_03", "dd/d35/_b_q24250_8h.html#a32432e5630258ca0a8c698d12357e2e6", null ],
    [ "BQ24250_REG_04", "dd/d35/_b_q24250_8h.html#a0b9c83b6f138161cc8e7251ab61be75f", null ],
    [ "BQ24250_REG_05", "dd/d35/_b_q24250_8h.html#aacc40bbb9038946bb23f70f0e9ea5093", null ],
    [ "BQ24250_REG_06", "dd/d35/_b_q24250_8h.html#a2dab2439615f0be4bf473790b21dfc7e", null ],
    [ "BQ24250_REG_07", "dd/d35/_b_q24250_8h.html#a39c9545813347a96bceb4d2d0b78df56", null ],
    [ "BQ24250_drv_init", "dd/d35/_b_q24250_8h.html#a335c7d217fbff18d29601f5f2cf997c7", null ],
    [ "BQ24250_drv_reg01_get", "dd/d35/_b_q24250_8h.html#a1d0fb7bbce391d2d0f317b29c0f7c2ad", null ],
    [ "BQ24250_drv_reg01_set", "dd/d35/_b_q24250_8h.html#a751e86b9d49dada265c7825747630f06", null ],
    [ "BQ24250_drv_reg02_get", "dd/d35/_b_q24250_8h.html#ab3e01600e342f011e193bd0e38b8fb01", null ],
    [ "BQ24250_drv_reg02_set", "dd/d35/_b_q24250_8h.html#afaf5f902022703e1cfb0367d6fdc8b76", null ],
    [ "BQ24250_drv_reg03_get", "dd/d35/_b_q24250_8h.html#acb20e468afe8d0ae49d4ed853d2cf44c", null ],
    [ "BQ24250_drv_reg03_set", "dd/d35/_b_q24250_8h.html#ae1aef98d98f4731aebfd471384464552", null ],
    [ "BQ24250_drv_reg04_get", "dd/d35/_b_q24250_8h.html#a8f82f7bcac53cbe331e56975ab9864e5", null ],
    [ "BQ24250_drv_reg04_set", "dd/d35/_b_q24250_8h.html#ad864a0153b9adce6fe55a8a7fbf5fa09", null ],
    [ "BQ24250_drv_reg05_get", "dd/d35/_b_q24250_8h.html#a722e3849b43b680553d59c795cd93de3", null ],
    [ "BQ24250_drv_reg05_set", "dd/d35/_b_q24250_8h.html#a8d173778d03a696d4276510cc62c2cc8", null ],
    [ "BQ24250_drv_reg06_get", "dd/d35/_b_q24250_8h.html#a7ceadb8f8185325cac08a5e5ec64f270", null ],
    [ "BQ24250_drv_reg06_set", "dd/d35/_b_q24250_8h.html#ad7e06335e613c259116e1c921269974d", null ],
    [ "BQ24250_drv_reg07_get", "dd/d35/_b_q24250_8h.html#a91edbdc3f25080a858f4c4f50b63bc3d", null ],
    [ "BQ24250_drv_reg07_set", "dd/d35/_b_q24250_8h.html#a7059253363dee0285fb6e7a042eaeb91", null ]
];