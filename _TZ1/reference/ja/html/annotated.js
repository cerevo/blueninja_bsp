var annotated =
[
    [ "MPU9250_accel_val", "d9/d82/struct_m_p_u9250__accel__val.html", "d9/d82/struct_m_p_u9250__accel__val" ],
    [ "MPU9250_gyro_val", "d6/dbf/struct_m_p_u9250__gyro__val.html", "d6/dbf/struct_m_p_u9250__gyro__val" ],
    [ "MPU9250_magnetometer_val", "dd/d89/struct_m_p_u9250__magnetometer__val.html", "dd/d89/struct_m_p_u9250__magnetometer__val" ],
    [ "MPU9250_temperature_val", "da/dd3/struct_m_p_u9250__temperature__val.html", "da/dd3/struct_m_p_u9250__temperature__val" ],
    [ "TZ01_SYSTEM_TICK", "d8/d94/struct_t_z01___s_y_s_t_e_m___t_i_c_k.html", "d8/d94/struct_t_z01___s_y_s_t_e_m___t_i_c_k" ],
    [ "TZ01_SYSTEM_TICK_INFO", "d5/d84/struct_t_z01___s_y_s_t_e_m___t_i_c_k___i_n_f_o.html", "d5/d84/struct_t_z01___s_y_s_t_e_m___t_i_c_k___i_n_f_o" ]
];