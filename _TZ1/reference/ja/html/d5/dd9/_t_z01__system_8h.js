var _t_z01__system_8h =
[
    [ "TZ01_SYSTEM_TICK_INFO", "d5/d84/struct_t_z01___s_y_s_t_e_m___t_i_c_k___i_n_f_o.html", "d5/d84/struct_t_z01___s_y_s_t_e_m___t_i_c_k___i_n_f_o" ],
    [ "TZ01_SYSTEM_TICK", "d8/d94/struct_t_z01___s_y_s_t_e_m___t_i_c_k.html", "d8/d94/struct_t_z01___s_y_s_t_e_m___t_i_c_k" ],
    [ "TZ01_system_RUNEVT", "d5/dd9/_t_z01__system_8h.html#a40827cbc3cd1b39ffdd44366ea0fbc2b", [
      [ "RUNEVT_NONE", "d5/dd9/_t_z01__system_8h.html#a40827cbc3cd1b39ffdd44366ea0fbc2baed4a4f29c92a14c726c2c4a55b3cb584", null ],
      [ "RUNEVT_LO_VOLT", "d5/dd9/_t_z01__system_8h.html#a40827cbc3cd1b39ffdd44366ea0fbc2baab6bb9c7cb5aa83a4dda4e52fd7ad7a8", null ],
      [ "RUNEVT_POWOFF", "d5/dd9/_t_z01__system_8h.html#a40827cbc3cd1b39ffdd44366ea0fbc2bafd89302a28231bef830234c2c59ec17a", null ]
    ] ],
    [ "TZ01_system_init", "d5/dd9/_t_z01__system_8h.html#a2236f3dfbd2dbe75119dd9406160ec2f", null ],
    [ "TZ01_system_run", "d5/dd9/_t_z01__system_8h.html#aee4cbfac4c422bcf0e91548767e8dc78", null ],
    [ "TZ01_system_tick_check_timeout", "d5/dd9/_t_z01__system_8h.html#af866d1c8534fb05269cde0b21c58fe53", null ],
    [ "TZ01_system_tick_clear", "d5/dd9/_t_z01__system_8h.html#acb51712c2a05526b94f1fb3a16fde858", null ],
    [ "TZ01_system_tick_is_active", "d5/dd9/_t_z01__system_8h.html#a552caa89554059ad1427cf3db4923482", null ],
    [ "TZ01_system_tick_start", "d5/dd9/_t_z01__system_8h.html#a2ac0b9386f2067336cdbcc730077843b", null ],
    [ "TZ01_system_tick_stop", "d5/dd9/_t_z01__system_8h.html#a00cb93ee116200c54d9632f15e8bb92d", null ]
];