@echo off
rem Launch OpenOCD
start "TZ10xx OpenOCD" /MIN %OCD_BIN_DIR%openocd -s %OCD_CFG_DIR% -f interface\cmsis-dap.cfg -f target\tz10xx.cfg
rem sleep 2

rem Launch GDB
start "%1 - GDB" /WAIT %TOOL_DIR%arm-none-eabi-gdb -ex "target remote localhost:3333" -ex "monitor reset init" %1

rem Terminate OpenOCD
taskkill /FI "WINDOWTITLE eq TZ10xx OpenOCD"
