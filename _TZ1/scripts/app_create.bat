@echo off
rem 
if "%~1" == "" goto usage
rem 
if exist %1 goto dir_exists

set DFP_VER=%~2
if "%DFP_VER%" == "" (
	for %%v in (%TZ1_BASE%sdk\*.patch) do (
		set V=%%~nv
	)
	goto DFP_VER_SET
) else (
	for %%v in (%TZ1_BASE%sdk\*.patch) do (
		if "%DFP_VER%" == "%%~nv" (
			goto DFP_VER_OK
		)
	)
)

goto dfp_ver_failed
:DFP_VER_SET
set DFP_VER=%V%

:DFP_VER_OK
echo %DFP_VER%

echo Create application directory: %1
mkdir %1

echo Copy skeleton files.
echo .doxyfile > app_create_exclude-list
xcopy %TZ1_BASE%skel\* %1\ /E /EXCLUDE:app_create_exclude-list
del /F app_create_exclude-list

echo Copy RTE files.
mkdir %1%\RTE
mkdir %1%\RTE\Device\TZ1001MBG\
mkdir %1%\RTE\Middleware\TZ1001MBG\
xcopy /E %SDK_DIR%TOSHIBA.TZ10xx_DFP.%DFP_VER%\RTE_Driver\Config\* %1%\RTE\Device\TZ1001MBG\
xcopy /E %SDK_DIR%TOSHIBA.TZ10xx_DFP.%DFP_VER%\Middleware\blelib\Config\* %1%\RTE\Middleware\TZ1001MBG\
xcopy /E %SDK_DIR%TOSHIBA.TZ10xx_DFP.%DFP_VER%\Middleware\TWiC\Config\* %1%\RTE\Middleware\TZ1001MBG\
copy %SDK_DIR%TOSHIBA.TZ10xx_DFP.%DFP_VER%\Boards\TOSHIBA\RBTZ1000\Template\RTE\RTE_Components.h %1%\RTE\
cd %1%
patch -p0 < %TZ1_BASE%sdk\%DFP_VER%.patch

sed "s/^TARGET.*$/TARGET=%1%/" Makefile > Makefile.new
sed "s/^DFP_VER.*$/DFP_VER=%DFP_VER%/" Makefile.new > Makefile
rm -rf Makefile.new

exit /B

:usage
echo usage: %0 APPNAME
exit /B

:dir_exists
echo %1 is already exists.
exit /B

:dfp_ver_failed
echo TOSHIBA.TZ10xx_DFP.%2 is invalid version.
echo Please choice from the list:
for %%v in (%TZ1_BASE%sdk\*.patch) do (
	echo * %%~nv
)
exit /B
