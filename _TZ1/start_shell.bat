@echo off

title CDP-TZ01B development tool

set TZ1_BASE=%~dp0
set GIT_BASE_DIR=""

set PATH=%SystemRoot%;%SystemRoot%\System32
set PATH=%PATH%;C:\MinGW\msys\1.0\bin;%TZ1_BASE%scripts;
if %GIT_BASE_DIR% == "" (
	if exist "%ProgramFiles%\Git\usr\bin" (
		rem Git for Windows 2.x
		set PATH=%PATH%;"%ProgramFiles%\Git\usr\bin";"%ProgramFiles%\Git\bin"
	) else if exist "%ProgramFiles(x86)%\Git\usr\bin" (
		rem Git for Windows 2.x Wow
		set PATH=%PATH%;"%ProgramFiles(x86)%\Git\usr\bin";"%ProgramFiles(x86)%\Git\bin"
	) else if exist "%ProgramFiles%\Git\bin" (
		rem Git for Windows 1.x
		set PATH=%PATH%;"%ProgramFiles%\Git\bin"
	) else if exist "%ProgramFiles(x86)%\Git\bin" (
		rem Git for Windows 1.x Wow
		set PATH=%PATH%;"%ProgramFiles(x86)%\Git\bin"
	)
) else (
	rem Git for Windows Custom path.
	set PATH=%PATH%;%GIT_BASE_DIR%
)

set TOOL_DIR=%TZ1_BASE%tools\bin\
set SDK_DIR=%TZ1_BASE%sdk\

set OCD_BIN_DIR=%TZ1_BASE%OpenOCD-0.8.0\bin\
set OCD_CFG_DIR=%TZ1_BASE%OpenOCD-0.8.0\share\openocd\scripts\

cmd
