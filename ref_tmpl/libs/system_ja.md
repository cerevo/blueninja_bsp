Systemライブラリ{#lib_system}
==
Systemライブラリは、BlueNinjaモジュールの動作に必要な機能を提供します。

## ソースコード
* TZ01_system.h
* TZ01_system.c

## 構成
![Systemライブラリ構成図](BlueNinja_libSystem_stracture_ja.png)

### System
ペリフェラルの初期化等と電源ボタン押下、ハートビート出力のイベントを処理します。

### Tick
1ms単位で時間経過を管理します。

tz01_system_conf.h の TZ01_SYSTEM_TICK_NO 列挙体でタイマーの定義をしています。
SYSTICK_NO_*はシステムライブラリで使用するタイマーですので削除しないでください。

### Power
電源ボタンの監視、電源ホールド、低電圧検出を行います。

Systemライブラリ内部で使用されており外部に公開していません。

## 依存するペリフェラル

### PMU
PMUでCPUとペリフェラルに供給するクロックを設定しています。

|              |クロックソース         | 周波数 |
|--------------|-----------------------|-------:|
|PMU_CSM_MAIN  |PMU_CLOCK_SOURCE_PLL   |   48MHz|
|PMU_CSM_UART1 |PMU_CLOCK_SOURCE_OSC12M|   12MHz|

|クロックドメイン| 分周 | 周波数 | 備考                                   |
|----------------|-----:|-------:|----------------------------------------|
|PMU_CD_MPIER    |    1 |  48MHz | CPUのクロックソース                    |
|PMU_CD_PPIER0   |   24 |   2MHz | TMR0を含むペリフェラルのクロックソース |
|PMU_CD_PPIER1   |    4 |  12MHz |                                        |
|PMU_CD_PPIER2   |    4 |  12MHz |                                        |
|PMU_CD_UART1    |    1 |  12MHz | UART1のクロックソース                  |

詳細については[ApPLite-2技術情報](http://toshiba.semicon-storage.com/jp/design-support/login/applite.html)の「TZ1000 Series Reference Manual MCU Power Management Unit」及びBSPの「TZ1000 Series Software manual PMU Driver」を参照してください。

### GPIO
GPIOのポート1、3、4、10を使用します。
用途は以下のようになります。

| ポートNo | 入出力 | 用途                 | Hi/Loの定義                        |
|---------:|:------:|----------------------|------------------------------------|
| 1        |入力    | 電源ボタン           | Lo: ON <br>Hi: OFF                 |
| 3        |出力    | 電源ホールド         | Lo: 未ホールド <br>Hi: ホールド    |
| 4        |入力    | 電源電圧低下検出     | Lo: 低電圧検出 <br>Hi: 低電圧未検出|
| 10       |出力    | 電源/ハートビートLED | Lo: 消灯 <br>Hi:点灯               |

### TMR
TMR0を32bitのフリーランニングタイマーとして使用します。

TMR0のクロックソースが2MHzに設定されていることを前提としており、最大1800000ミリ秒(30分)の時間経過を計測できます。
