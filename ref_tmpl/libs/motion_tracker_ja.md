MotionTrackerライブラリ{#lib_motion_tracker}
==
MotionTrackerライブラリは、BlueNinjaモジュールの9軸モーションセンサーからのデータ取得、姿勢等の計算の機能を提供します。

## ソースコード
* TZ01_motion_tracker.h
* TZ01_motion_tracker.c

## 構成
![MotionTrackerライブラリ構成図](BlueNinja_LibMotionTracker_stracture_ja.png)

9軸モーションセンサーの角速度を取得するGyro、加速度を取得するAccel、地磁気を取得するMagnetometer。<br>
姿勢等の計算を行うComputeで構成されます。

現時点でComputeには、加速度から姿勢角を計算する TZ01_motion_tracker_compute_axis_angle() が実装されています。

## 依存するペリフェラル
### SPI
SPI3を9軸モーションセンサーのInvensens MPU-9250との接続に使用します。
