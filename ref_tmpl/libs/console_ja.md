Consoleライブラリ{#lib_console}
==
Consoleライブラリは、簡易的なシリアルコンソールの機能を提供します。

## ソースコード
* TZ01_console.h
* TZ01_console.c

## 構成
![Consoleライブラリ構成図](BlueNinja_LibConsole_stracture_ja.png)
### String/Char
文字や文字列を扱う関数群です。
* TZ01_console_putc()
* TZ01_console_getc()
* TZ01_console_puts()
* TZ01_console_gets()

### Byte
バイト列を扱う関数群です。
* TZ01_console_write()
* TZ01_console_read()

## 依存するペリフェラル
### UART
UART1をシリアルコンソールとして使用します。

UART1はブレイクアウトボードのインターフェースに接続されます。<br>
インターフェースボードでUSB-Serial変換が行われます。
