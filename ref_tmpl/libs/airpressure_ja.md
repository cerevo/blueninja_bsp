Airpressureライブラリ{#lib_airpressure}
==
Airpressureライブラリは、BlueNinjaモジュールの気圧センサーからのデータ取得機能を提供します。

## ソースコード
* TZ01_airpressure.h
* TZ01_airpressure.c

## 構成
![Airpressureライブラリ構成図](BlueNinja_LibAirpressure_stracture_ja.png)

気圧を取得するAirpressure、温度を取得するTemperatureで構成されます。

## 依存するペリフェラル
### I2C
I2C1を気圧センサBOSH BMP280との接続に使用します。
