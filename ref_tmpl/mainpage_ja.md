概要{#mainpage}
==

本ドキュメントは、BlueNinja(CDP-TZ01B)のBoard Support Pacageのリファレンスマニュアルです。  

## 対象範囲
BlueNinjaのソフトウェアの構成を以下に示します。

![BlueNinjaソフトウェア構成](BlueNinja_software_structure_ja.png)

本ドキュメントは、図中の「BlueNinja BSP」の範囲を対象としています。<br>
「TOSHIBA TZ1000 BSP」についてはBSPの付属のドキュメント及び[ApPLite-2技術情報ページ](http://toshiba.semicon-storage.com/jp/design-support/login/applite.html)のドキュメントを参照してください。

## BlueNinja機能仕様
BlueNinjaモジュール、ブレイクアウトボードの機能仕様は[BlueNinja機能仕様](@ref spec_blueninja)を参照してください。

## BlueNinja BSPの導入
導入手順は別紙「BlueNinja CDP-TZ01B スターターガイド」を参照してください。

## BlueNinja BSPの構成
BlueNinja BSPは、BlueNinjaに搭載されたデバイスのドライバーとサポートライブラリから構成されています。

### ドライバー
BlueNinjaに搭載しているデバイスのドライバです。

| デバイス | 機能 | ソースコード |
|----------|------|----------------|
| Invensens MPU-9250 | 9軸モーションセンサー |  MPU-9250.h <br> MPU-9250.c |
| BOSH BMP280 | 気圧センサー | BMP280.h <br> BMP280.c |
| Ti BQ24250 | LiPoバッテリー充電IC | BQ24250.h <br> BQ24250.c |

### ライブラリ
アプリケーションからBluneNinjaの機能を使用するためのライブラリです。<br>
モジュール上のLED、電源関連のGPIOの定義、タイマーの定義、シリアルコンソールの設定は tz01_system_conf.h で行います。

| ライブラリ | 機能 |
|------------|------|
| [System](@ref lib_system) | 電源管理(電源ホールド、電源スイッチ、ハートビートLED)、タイマー管理 |
| [Console](@ref lib_console) | シリアルコンソール |
| [Battery Charger](@ref lib_battery_charger) | バッテリーチャージャ |
| [Airpressure](@ref lib_airpressure) | 気圧センサー |
| [Motion Tracker](@ref lib_motion_tracker) | モーショントラッカー |

## 関連情報
* [BlueNinja製品ページ](http://blueninja.cerevo.com)
* [BlueNinja BSP](https://bitbucket.org/cerevo/blueninja_bsp/)
* [ApPLite-2技術情報](http://toshiba.semicon-storage.com/jp/design-support/login/applite.html)
