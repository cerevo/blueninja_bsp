#
# TZ10xx System reset functions
# - Initialize peripherals.
#
proc init_dcdc {} {
	#1.
	mww 0x400001C0 0x00010000;	# CG_OFF_HARDMACRO
}

proc init_voltage_mode {} {
	#2.
	set stat(0) 0x01;
	while {[expr $stat(0) & 0x01] != 0} {
		mem2array stat 32 0x40000700 1;	#MOVE_VOLTAGE_START
	}

	mww 0x40000730 0x00000000;	# VOLTAGEMODE_SETTING
	mww 0x40000704 0x00000000;	# MOVE_POWER_VOLTAGE_MODE
	mww 0x40000700 0x00000001;	# MOVE_VOLTAGE_START

	set stat(0) 0x01;
	while {[expr $stat(0) & 0x01] != 0} {
		mem2array stat 32 0x40000700 1;	#MOVE_VOLTAGE_START
	}
}

proc init_clock_source {} {
	#3.
	mww 0x40000404 0x00000000;	# CSM_MAIN
	mww 0x40000408 0x00000000;	# CSM_CPUTRC
	mww 0x4000040C 0x00000000;	# CSM_CPUST
	mww 0x40000414 0x00000000;	# CSM_UART0
	mww 0x40000418 0x00000000;	# CSM_UART1
	mww 0x4000041C 0x00000000;	# CSM_UART2
	mww 0x40000420 0x00000000;	# CSM_ADCC12A
	mww 0x40000424 0x00000000;	# CSM_ADCC24A
}

proc init_prescaler {} {
	#4.
	mww 0x40000484 0x00111111;	# PRESCAL_MAIN
	mww 0x4000048C 0x00000001;	# PRESCAL_CPUST
	mww 0x40000494 0x00000001;	# PRESCAL_UART0
	mww 0x40000498 0x00000001;	# PRESCAL_UART1
	mww 0x4000049C 0x00000001;	# PRESCAL_UART2
	mww 0x400004A0 0x00000001;	# PRESCAL_ADCC12A
	mww 0x400004A4 0x00000001;	# PRESCAL_ADCC24A
}

proc init_and_poweron_pll {} {
	#5
	mem2array stat 32 0x40000500 1;	# CONFIG_OSC12M.OSC12M_EN
	if {$stat(0) == 0} {
		# OSC12M stopped.
		mww 0x40000500 0x00000001;	# CONFIG_OSC12M
		sleep 10;
	}

	mem2array stat 32 0x40002444 1;	# PSW_PLL
	if {$stat(0) == 0} {
		mww 0x40002444 0x00000001;	# PSW_PLL
		mww 0x40002444 0x00000003;	# PSW_PLL
	}

  	mem2array stat 32 0x40002544 1;	# ISO_PLL
	if {$stat(0) == 3} {
		mww 0x40002544 0x00000000;	# ISO_PLL
	}

	mww 0x40000508 0x80000001;	# CONFIG_PLL_0
	mww 0x4000050c 0x00000093;	# CONFIG_PLL_1
	sleep 1;
	mww 0x40000508 0x00000000;	# CONFIG_PLL_0
	sleep 1;
}

proc start_usb {} {
	#6.
	mww 0x40000410 0x00000000;	# CSM_USBI
	#7.
	mww 0x40000490 0x00000001;	# PRESCAL_USBI
}

proc init_pu {} {
	# 8. clock and reset
	mww 0x4000032C 0x0000000d;	# SRST_ON_PU
	mww 0x4000032C 0x00000010;	# SRST_ON_PU
	mww 0x40000000 0x00000100;	# CG_ON_POWERDOMAIN

	# 9. isolation
	mww 0x40002520 0x00000003;	# ISO_PU

	# 10. turn off power-switches
	mww 0x40002450 0x00000000;	# PSW_IO_USB
	mww 0x40002420 0x00000000;	# PSW_PU
	mww 0x40002454 0x00000000;	# PSW_HARDMACRO
}

proc stop_usb {} {
	# 11.
	mww 0x40000490 0x00000000;	# PRESCAL_USBI
}

proc init_clock_source_main {} {
	# 12.
  	mem2array stat 32 0x40002544 1;	# EFUSE_BOOTSEQ
	set seq [expr $stat(0) & 3];
	if {$seq == 0} {
		mem2array stat 32 0x40000500 1;	# CONFIG_OSC_12M.OSC12M_EN
		if {[expr $stat(0) & 0x01] == 0} {
			mww 0x40000500 0x00000001;	# OSC12M_EN
			sleep 10;
		}

		mem2array stat 32 0x40002444 1;	# PSW_PLL
		if {[expr $stat(0) & 0x03] == 0} {
			mww 0x40002444 0x00000001;	# PSW_PLL
			mww 0x40002444 0x00000003;	# PSW_PLL
		}

		mem2array stat 32 0x40002544 1;	# ISO_PLL
		if {[expr $stat(0) & 0x03] == 3} {
          		mww 0x40002544 0x00000000;	# ISO_PLL
		}

		mem2array stat 32 0x40000508 1;	# CONFIG_PLL_0.PLL_BP
		if {[expr $stat(0) & 0x01] == 1} {
			mww 0x40000508 0x00000001;	# CONFIG_PLL_0
			sleep 1;
			mww 0x40000508 0x00000000;	# CONFIG_PLL_0
			sleep 1;
		}
		mww 0x40000404 0x00000002;	# CSM_MAIN
	} elseif {$seq == 1} {
		mem2array stat 32 0x40000500 1;	# OSC12M_EN
		if {[expr $stat(0) & 0x01] == 0} {
			mww 0x40000500 0x00000001;	# OSC12M_EN
			sleep 10;
		}

		mww 0x40000404 0x00000001;	# CSM_MAIN

		mem2array stat 32 0x40000508 1;	# CONFIG_PLL_0.PLL_BP
		if {[expr $stat(0) & 0x01] == 0} {
			mww 0x40000508 0x00000001;	# CONFIG_PLL_0
		}

		mem2array stat 32 0x40002544 1;	# ISO_PLL
		if {[expr $stat(0) & 0x03] == 0} {
			mww 0x40002544 0x00000003;	# ISO_PLL
		}

		mem2array stat 32 0x40002444 1;	# PSW_PLL
		if {[expr $stat(0) & 0x03] == 3} {
			mww 0x40002444 0x00000000;	# PSW_PLL
		}
	} else {
		mww 0x40000404 0x00000000;	# CSM_MAIN

		mem2array stat 32 0x40000500;	# CONFIG_OSC_12M.OSC12M_EN
		if {[expr $stat(0) & 0x01] == 0} {
			return;
		}

		mww 0x40000500 0x00000000;	# CONFIG_OSC_12M

		mem2array stat 32 0x40000508;	# CONFIG_PLL_0.PLL_BP
		if {[expr $stat(0) & 0x01] == 0} {
			mww 0x40000508 0x00000001;	# CONFIG_PLL_0
		}

		mem2array stat 32 0x40002544;	# ISO_PLL
		if {[expr $stat(0) & 0x03] == 0} {
			mww 0x40002544 0x00000003;	# ISO_PLL
		}

 		mem2array stat 32 0x40002444 1;	# PSW_PLL
		if {[expr $stat(0) & 0x03] == 3} {
			mww 0x40002444 0x00000000;	# PSW_PLL
		}
	}
}

proc power_on_domain {} {
	#13-1.
	set stat(0) 0x01;
	mww 0x40000714 0x00000000;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x00000e00;	# POWERDOMAIN_CTRL
	while {[expr $stat(0) & 0xffff3003] != 0} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}

	#13-2.
	set stat(0) 0x01;
	mww 0x40000714 0x00000000;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x000000a4;	# POWERDOMAIN_CTRL
	while {[expr $stat(0) & 0xfffffc33] != 0} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}
	
	#13-3.
	set stat(0) 0x01;
	mww 0x40000714 0x00000000;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x00000012;	# POWERDOMAIN_CTRL
	while {[expr $stat(0) & 0xffffff3f] != 0} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}

	#13-4.
	set stat(0) 0x01;
	mww 0x40000714 0x00000000;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x00000008;	# POWERDOMAIN_CTRL
	if {$stat(0) != 0} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}
}

proc init_iocell {} {
	#14
	mww 0x40002308 0x00000000;	# CTRL_IO_AON_2
	mww 0x4000230C 0x00000000;	# CTRL_IO_AON_3
	#15
	mww 0x40002318 0x00000000;	# CTRL_IO_AON_6
	mww 0x40002314 0x00000000;	# CTRL_IO_AON_5
	mww 0x40002310 0x00000001;	# CTRL_IO_AON_4
}

proc reset_on_domain {} {
	#16
	mww 0x40000020 0xfffffffe;	# SRST_ON_POWERDOMAIN
	#17
	mww 0x40000300 0xfeffffff;	# SRST_ON_PM0
	mww 0x40000304 0xffffffff;	# SRST_ON_PM1
	mww 0x40000308 0xffffffff;	# SRST_ON_PM2
	#18
	mww 0x40000000 0x00000f82;	# CG_ON_POWERDOMAIN
	#19
	mww 0x40000100 0x00500000;	# CG_ON_PM_0
	mww 0x40000104 0xffffffff;	# CG_ON_PM_1
	mww 0x40000108 0xffffffff;	# CG_ON_PM_2
	#20
	mww 0x40001124 0x00000004;	# CG_ON_PC_SCRT
	#21
	mww 0x40000004 0x0000003c;	# CG_OFF_POWERDOMAIN
	#22
	mww 0x40000024 0x00000020;	# SRST_OFF_POWERDOMAIN
	#23
	mww 0x40000180 0x08800000;	# CG_OFF_PM_0
	#24
	mww 0x40000380 0x09000000;	# SRST_OFF_PM_0
}

proc power_off_domain {} {
	#25-1
	set stat(0) 0x01;
	mww 0x40000714 0x00144004;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x00000600;	# POWERDOMAIN_CTRL
	while {$stat(0) != 0x00140000} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}

	#25-2
	set stat(0) 0x01;
	mww 0x40000714 0x00144004;	# POWERDOMAIN_CTRL_MODE
	mww 0x40000710 0x00000082;	# POWERDOMAIN_CTRL
	while {$stat(0) != 0x00144004} {
		mem2array stat 32 0x40000718 1;	# POWERDOMAIN_CTRL_STATUS
	}
}

proc stop_prescaler {} {
	#26
	mww 0x40000484 0x00000011;	# PRESCAL_MAIN
	mww 0x4000048C 0x00000001;	# PRESCAL_CPUST
	mww 0x40000494 0x00000000;	# PRESCAL_UART0
	mww 0x40000498 0x00000000;	# PRESCAL_UART1
	mww 0x4000049C 0x00000000;	# PRESCAL_UART2
	mww 0x400004A0 0x00000000;	# PRESCAL_ADCC12A
	mww 0x400004A4 0x00000000;	# PRESCAL_ADCC24A
}

proc init_adpll {} {
	#27
	mww 0x40000510 0x00007008;	# CONFIG_ADPLL_0
	mww 0x40000514 0x00000000;	# CONFIG_ADPLL_1
	#28
	mww 0x40002548 0x00000003;	# ISO_ADPLL
	#29
	mww 0x40002448 0x00000000;	# PSW_ADPLL
}

proc init_rtc {} {
	#30
	mww 0x40000148 0xffffffff;	# CG_ON_REFCLK
	#31
	mww 0x40002020 0xffffffff;	# SRST_ON_PA
	sleep 1;
	#32
	mww 0x40002000 0xffffffff;	# CG_ON_PA
	sleep 1;
	#33
	mww 0x40002080 0x00000000;	# CSM_RTC
	#34
	mww 0x40002100 0x00000000;	# CONFIG_OSC32K
	mww 0x40002104 0x00000000;	# CONFIG_SiOSC32K
}

proc init_macro {} {
	# 35
	mww 0x400005C0 0x00000000;	# SELECT_EFUSE
	mww 0x40000580 0x00000000;	# OVERRIDE_EFUSE_OSC12M
	mww 0x40002180 0x00000000;	# OVERRIDE_EFUSE_OSC32K
	mww 0x40002184 0x00000000;	# OVERRIDE_EFUSE_SiOSC32K
	mww 0x40002190 0x00000000;	# OVERRIDE_EFUSE_BGR_0
	mww 0x40002194 0x00000000;	# OVERRIDE_EFUSE_BGR_1
	mww 0x40000528 0x00000000;	# CONFIG_DCDC_LVREG_1
	mww 0x40002108 0x00000011;	# CONFIG_SiOSC4M
	mww 0x40002188 0x00000000;	# OVERRIDE_EFUSE_SiOSC4M
	mww 0x40002108 0x00000001;	# CONFIG_SiOSC4M
}

proc init_misc_registers {} {
	# 36
	mww 0x40000010 0x00000000;	# DCG_POWERDOMAIN
	mww 0x40000290 0x00020002;	# CLKREQ_CONFIG_PE
	mww 0x40000720 0xffffffff;	# POWERDOMAIN_CTRL_MODE_FOR_WAIT
	mww 0x40000724 0x00a8abf8;	# POWERDOMAIN_CTRL_MODE_FOR_WRET
	mww 0x40000728 0x00a8aaa8;	# POWERDOMAIN_CTRL_MODE_FOR_RET
	mww 0x40000740 0x001e001e;	# WAITTIME_LDOF
	mww 0x40000744 0x000f000f;	# WAITTIME_PSW
	mww 0x40000748 0x00a01e1e;	# WAITTIME_DVSCTL
	mww 0x40000780 0x00000000;	# POWERMODE_SLEEP_CG_ON
	mww 0x40000790 0x00000000;	# POWERMODE_SLEEP_PRESCAL
	mww 0x400020c0 0x00000000;	# RTCLV_RSYNC_SETTING
	mww 0x40002210 0x00000000;	# BROWNOUTMODE
	mww 0x40002700 0x00000000;	# IRQ_SETTING_0
	mww 0x40002704 0x00000000;	# IRQ_SETTING_1
	mww 0x40002708 0xffffffff;	# IRQ_STATUS
	mww 0x4000270C 0x00000000;	# WAKEUP_EN
}

proc tz10xx_sys_reset {} {
	init_dcdc
	init_voltage_mode
	init_clock_source
	init_and_poweron_pll
	start_usb
	init_pu
	stop_usb
	init_clock_source_main
	init_prescaler
	power_on_domain
	init_iocell
	reset_on_domain
	power_off_domain
	stop_prescaler
	init_adpll
	init_rtc
	init_macro
	init_misc_registers
}

proc tz10xx_enable_swo {} {
	#EnableTraceClock
	mww 0x40000484 0x00001011 ;#Set APB0 prescaler at 1.
	mww 0x40000184 0x00000003 ;#Gate off H2APBP0 and H2HSYNCDNP0 clock.
	mww 0x40000188 0x00000001 ;#Gate off GCONF clock.
	mww 0x40000384 0x00000003 ;#Reset off H2APBP0 and H2HSYNCDNP0.
	mww 0x40000388 0x00000001 ;#Reset off GCONF.
	mww 0x40000408 0x00000000 ;#Set trace clock at 4MHz.
	mww 0x400011A4 0x00000004 ;#Enable Trace Clock
	mww 0x4004A0C4 0x00000001 ;#binary count enable
}

